### HOTFIX - 27-03-2023
* Fixed All Enhancements.
* Improved Base
### LAPRAS - 27-03-2023
* Fixed Updating Module From Magisk Module Section.
* Fixed Confliction Between Safety Net And High Res Audio.
* Changed config file extension to "txt".
* Removed Enahnce Audio And Video, Security Patch, Ram Enhancement, Safety Net Fix, High Res Audio From Config, All will enable by default.
* Improved Scripts for future updates.
* Reworked on System Props.
* Miscellaneous Changes.
